import { DialogService } from './services/dialog.service';
import {
  Component, 
  OnInit,
  AfterViewChecked,
  HostListener,
  ViewChild,
  ElementRef,
  ViewContainerRef,
  ComponentFactoryResolver,
  ComponentFactory
} from "@angular/core"

import { URLs, oMsg, secretToken } from './util/values';
import { TranslateService } from './util/translate/translate.service';
import {HttpClient} from "@angular/common/http";
import {
  ngIfSlide,
  ngIfFade,
  fade,
  ngIfScale
} from "./util/animates.custons"
import { Router } from '@angular/router';
import {Location} from "@angular/common";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [ngIfSlide, ngIfFade, fade, ngIfScale]
})

export class AppComponent implements OnInit, AfterViewChecked{
  constructor(
    private translate: TranslateService,
    private http: HttpClient,
    private router: Router,
    private location: Location,
    private resolver: ComponentFactoryResolver
  ){}
  

  public tittle: string;
  public links = [];
  public actions = [];
  public actionsFixed = [];
  public userLogged = {nome: "", login: "", senha: ""};
  public toolbarShow = "out";
  public isRoot = true;
  public isLogged = false;
  private roots = ["/", "/home"];
  public textSair = this.translate.getValue("SAIR");
  
  ngOnInit() {
    this.setUserLogged();
    this.paginaAtiva(null);
    let nav = document.querySelector(".navbar-fixed");
  }

  @ViewChild("navBar") navBar: ElementRef;
  
  ngAfterViewChecked(){
  }


  // @HostListener('window:scroll', ['$event'])
  // public onWindowScroll(event: Event): void {
  //   console.log(this.navBar);
  // }

  public setUserLogged(){
    if(!this.userLogged["login"]){
      this.http.get(URLs.localhost + URLs.AUTH_LOGGED).subscribe(data => {
        this.userLogged = data[oMsg.OBJ];
        this.isLogged = true;
      }, error => {
          this.isLogged = false;
          sessionStorage.removeItem(secretToken.TOKEN);
      });
    }
  }

  public logout(){
    this.isLogged = false;
    sessionStorage.removeItem(secretToken.TOKEN);
    this.userLogged = null;
    this.router.navigate(["login"]);
  }

  public paginaAtiva(page){
      let urlAtual =  this.router.url;
      if(this.roots.indexOf(urlAtual) > -1) {
        this.isRoot = true;
      }else{
        this.isRoot = false;
      }
      this.cleanActions();
      this.cleanActionsFixed();
  }

  public backPage(){
    this.location.back();
  }

  public setTitle(title){
    setTimeout(() => {
      this.tittle = title;
    }, 200);
  }

  public addActionFixed(id: string, icon: string, toolTip: string, action: Function){
    let notCan = this.actionsFixed.some( (a) => {
      return a.id == id 
    });
    if(!notCan){
      this.actionsFixed.push({id: id, icon: icon, tooltip: toolTip, action: action});
    }
  }

  public addAction(id: string, icon: string, toolTip: string, action: Function){
    let notCan = this.actions.some( (a) => {
      return a.id == id 
    });
    if(!notCan){
      this.actions.push({id: id, icon: icon, tooltip: toolTip, action: action});
    }
  }
  public clickAction(action: Function){
    if(action){
      action();
    }
  }

  public removeAction(id: string){
    this.actions = this.actions.filter((action) => {
      return action.id != id;
    });
  }

  public removeActionFixed(id: string){
    this.actions = this.actions.filter((action) => {
      return action.id != id;
    });
  }
  
  public cleanActions(){
    this.actions = [];
  }
  public cleanActionsFixed(){
    this.actionsFixed = [];
  }

}