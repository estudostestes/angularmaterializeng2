import { UtilService } from './util.service';
import { secretToken } from './../util/values';
import { Injectable } from '@angular/core';
import { 
    HttpEvent, 
    HttpInterceptor, 
    HttpHandler, 
    HttpRequest,
    HttpResponse,
    HttpErrorResponse 
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    constructor(
    ) {}

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let newReq = this.setTokenSessionStory(req);
        
        return next.handle(newReq).do(res => {
            if (res instanceof HttpResponse) {
            }
            
        }, error => {
            if(error instanceof HttpErrorResponse){
                if(error.status == 401){
                }
            }
        });
    }

    private setTokenSessionStory(req: HttpRequest<any>): HttpRequest<any>{
        let token = sessionStorage.getItem(secretToken.TOKEN);
        if(token){
            let newReq = req.clone({
                headers: req.headers.set(secretToken.TOKEN, token)
            });
            return newReq;
        }else{
            return req;
        }
    }
    
    private saveTokenSessionStory(res: HttpResponse<any>): void{
        console.log(res);
        let token = res.headers[secretToken.TOKEN]
        if(token){
            sessionStorage.setItem(secretToken.TOKEN, token);
        }
    }
}