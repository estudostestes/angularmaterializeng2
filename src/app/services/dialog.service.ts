import { TranslateService } from './../util/translate/translate.service';
import { UtilService } from './util.service';
import { secretToken, tMsg, oMsg } from './../util/values';
import {
    Injectable,
    ComponentRef,
    ComponentFactoryResolver,
} from '@angular/core';
import {
    MzModalService,
    MzBaseModal,
    MzModalComponent,
    MzToastService,
} from 'ng2-materialize';
import {HttpErrorResponse} from '@angular/common/http';
//Modals
import { LoadingComponent } from './../pages/custom-components/loading/loading.component';
import { ConfirmComponent } from './../pages/custom-components/confirm/confirm.component';
import { AlertComponent } from './../pages/custom-components/alert/alert.component';

@Injectable()
export class DialogService {
    constructor(
        private mzModalService: MzModalService,
        private mzToastService: MzToastService,
        private translateService: TranslateService,
        private utilService: UtilService,
        private factoryResolver: ComponentFactoryResolver
    ) {
        this.tituloModal = this.translateService.getValue("MENSAGEM");
    }

    private tituloModal: string = this.translateService.getValue("MENSAGEM");
    public loading: ComponentRef<MzBaseModal>;

    public alert(title: string, message: string){
        if(!message){message = ""}
        if(!title){title = this.tituloModal}
        console.log("Title: " + title);
        AlertComponent.title = title;
        AlertComponent.message = message;
        let modal = this.mzModalService.open(AlertComponent);
    }
    
    public confirm(title: string, message: string, calbackOk: any, callbackCancel: any){
        if(!message){message = ""}
        if(!title){title = this.tituloModal}
        if(!calbackOk){calbackOk = null}title
        if(!callbackCancel){callbackCancel = null}
        ConfirmComponent.title = title;
        ConfirmComponent.message = message;
        ConfirmComponent.callbackOk = calbackOk;
        ConfirmComponent.callbackCancel = callbackCancel;
        let modal = this.mzModalService.open(ConfirmComponent);
    }

    public openLoading(message: string, duration: number){
        if(!message){message = this.translateService.getValue("AGUARDE") + "..."}
        LoadingComponent.message = message;
        setTimeout(() => {
            if(!this.loading){
                this.loading = this.mzModalService.open(LoadingComponent);
            }
        }, 100);
        if(duration > 0){
            setTimeout(() => {
                this.closeLoading();
            }, duration);
        }
    }
    
    public closeLoading(){
        setTimeout(() => {
            console.log(this.loading);
            if(this.loading){
                console.log("Chamou!");
                this.loading.instance.modalComponent.close();
            }
        }, 100);
    }

    public setMessageLoading(message: string){
        LoadingComponent.message = message;
    }

    
    public toast(message: string, duration: number, addClass: string, callbackFinish){
        this.mzToastService.show(message, duration, addClass, callbackFinish);
    }
    
    public showErrorModal(error:any){
        this.closeLoading();
        sessionStorage.removeItem(secretToken.TOKEN);
        if(error instanceof HttpErrorResponse){
            let msg = this.utilService.getMsgStatusError(error.status);
            this.alert(this.tituloModal, msg);
        }
    }

    public showMsgData = function(data){
        let msgs = data[oMsg.LIST_MSG];
        let msgsModal = "";
        if(msgs){
            msgs.forEach(m => {
                 let isToast = false;
                 let msg: string;
                 if(m.type == tMsg.INFO){isToast = true; msg = m.msg;}
                 else if(m.type == tMsg.SUCCESS){isToast = true; msg = m.msg;}
                 else if(m.type == tMsg.DANGER){msg = m.msg;}
                 else if(m.type == tMsg.ALERT){msg = m.ms;}
                 msg = this.translateService.getValue(msg);
                 if(isToast){
                     this.toast(msg, 5000, null, null);
                 }else{
                    msgsModal += msg + "\n";
                }
            });
            if(msgsModal){
                this.alert(this.tituloModal, msgsModal);
            }
        }
        this.closeLoading();
    };
}