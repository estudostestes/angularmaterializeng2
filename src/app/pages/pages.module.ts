import { DialogService } from './../services/dialog.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from './../util/translate/translate.module';
import {FormsModule} from "@angular/forms"
import {
  BrowserAnimationsModule
} from '@angular/platform-browser/animations';
import 'rxjs/add/operator/do';
import { Observable } from 'rxjs/Observable';
import { HttpClientModule } from '@angular/common/http';
import {MaterializeModule} from "ng2-materialize";
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from './../services/auth.interceptor';
import { AuthGuard } from './../services/auth.guard';
import { Routes, RouterModule } from '@angular/router';
import { UtilService } from './../services/util.service';
//Paginas 
import { HomeComponent } from './home/home.component'
import { LoginComponent } from './login/login.component';
import { ListarUsuariosComponent } from './usuario/listar-usuario/listar-usuarios.component';
import { ListarOrganizacoesComponent } from './controle-acesso/organizacao/listar-organizacoes/listar-organizacoes.component';
import { AlertComponent } from './custom-components/alert/alert.component';
import { ConfirmComponent } from './custom-components/confirm/confirm.component';
import { LoadingComponent } from './custom-components/loading/loading.component';
import { TableComponent } from './custom-components/table/table.component';
import { HeadTableComponent } from './custom-components/table/head-table/head-table.component';
import { CadastroOrganizacaoComponent } from './controle-acesso/organizacao/cadastro-organizacao/cadastro-organizacao.component';
import { MenuRight } from './custom-components/menu-right/menu-right.component';
import { ListarPerfisComponent } from './controle-acesso/perfil/listar-perfis/listar-perfis.component';
import { CadastroPerfilComponent } from './controle-acesso/perfil/cadastro-perfil/cadastro-perfil.component';


@NgModule({
  imports: [
    RouterModule,
    CommonModule,
    FormsModule,
    BrowserAnimationsModule,
    TranslateModule,
    HttpClientModule,
    MaterializeModule.forRoot()
  ],
  declarations: [
    LoginComponent,
    HomeComponent,
    ListarUsuariosComponent,
    ListarOrganizacoesComponent,
    AlertComponent,
    ConfirmComponent,
    LoadingComponent,
    TableComponent,
    HeadTableComponent,
    CadastroOrganizacaoComponent,
    MenuRight,
    ListarPerfisComponent,
    CadastroPerfilComponent
  ],
  exports: [
    BrowserAnimationsModule,
    HttpClientModule,
    MaterializeModule,
    LoginComponent,
    HomeComponent,
    ListarUsuariosComponent,
    AlertComponent,
    ConfirmComponent,
    LoadingComponent,
    TableComponent,
    HeadTableComponent,
    MenuRight
  ],
  entryComponents: [
    ConfirmComponent,
    AlertComponent,
    LoadingComponent
  ],
  providers: [
    AuthGuard,
    UtilService,
    DialogService,
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true}
  ]
})
export class PagesModule { }
