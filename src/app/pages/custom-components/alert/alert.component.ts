import { MzBaseModal } from 'ng2-materialize';
import {
   Component,
    OnInit,
    ViewChild,
    ElementRef,
    AfterViewChecked
} from '@angular/core';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent extends MzBaseModal implements OnInit, AfterViewChecked {
  constructor(){
    super();
  }
  static title: string;
  static message: string;
  private btnOk: ElementRef;

 @ViewChild('btnOk') set setBtnOk(button: ElementRef) {
    this.btnOk = button;
 }

  ngOnInit() {
  }

  ngAfterViewChecked(){
    if(this.btnOk){
      this.btnOk.nativeElement.focus();
    }
  }

  get statictitle(){
    return AlertComponent.title;
  }

  get staticMessage(){
    return AlertComponent.message;
  }

}
