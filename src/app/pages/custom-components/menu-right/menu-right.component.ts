import { ngIfSlideOutRight, ngIfSlideInRight } from './../../../util/animates.custons';
import {
   Component,
    OnInit,
    Input,
    Output,
    EventEmitter,
    HostListener,
    Renderer2,
    ElementRef
  } from '@angular/core';

@Component({
  selector: 'm-menu-right',
  templateUrl: './menu-right.component.html',
  styleUrls: ['./menu-right.component.scss'],
  animations: [ngIfSlideOutRight, ngIfSlideInRight]
})
export class MenuRight implements OnInit {

  constructor(
    private elementRef: ElementRef,
    private renderer: Renderer2
  ) { }

  ngOnInit() {
    if(!this.width){
      this.width="21rem";
    }
    
  }

  @Input() isShow: boolean = false;
  @Input() title: string;
  @Input() width: string;
  @Input() full: boolean;
  @Output() close: EventEmitter<any> = new EventEmitter();

  public animateContainer:any;
  public animateMove:any;

  public closeClick(){
    this.isShow = false;
    this.close.emit(this.isShow);
  }

  public scrollSlideBody(event){
    if(event.target){
      let containerTitle = this.elementRef.nativeElement.querySelector(".side-slide-title");
      let nameClass = "shadow-bottom";
      if(event.target.scrollTop >= 1){
        this.renderer.addClass(containerTitle, nameClass);
      }else{
        this.renderer.removeClass(containerTitle, nameClass);
      }
    }
  }
}
