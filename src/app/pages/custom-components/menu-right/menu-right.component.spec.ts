import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SideslideComponent } from './sideslide.component';

describe('SideslideComponent', () => {
  let component: SideslideComponent;
  let fixture: ComponentFixture<SideslideComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SideslideComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SideslideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
