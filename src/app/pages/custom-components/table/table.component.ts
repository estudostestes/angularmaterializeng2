import { AppComponent } from './../../../app.component';
import { DialogService } from './../../../services/dialog.service';
import { oMsg } from './../../../util/values';
import { ngIfFade, ngIfSlide } from './../../../util/animates.custons';
import {
  Component,
  OnInit,
  Input,
  EventEmitter,
  Output, 
  ElementRef,
} from '@angular/core';
import { HeadTableComponent } from './head-table/head-table.component';
@Component({
  selector: 'm-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
  animations:[ngIfFade, ngIfSlide]
})
export class TableComponent implements OnInit {

  constructor(
    private DialogService: DialogService,
    private app: AppComponent
  ) { 
    this.heads = new Array();
  }

  // --------- Entradas
  @Input() data:any;
  @Input() list: Array<any>;
  @Input() addClass: string;
  @Input() isMult: boolean = false;

  // --------- Saídas
  @Output() loadTable: EventEmitter<any> = new EventEmitter();
  @Output() onClick: EventEmitter<any> = new EventEmitter();
  @Output() ondbClick: EventEmitter<any> = new EventEmitter();

  private CRESCENTE = "asc";
  private DECRESCENTE = "desc";
  private FIELD_SORT_DEFAULT = "_id";
  public pagination = {page: 1, limit: 10, fieldSort: this.FIELD_SORT_DEFAULT, sort: this.CRESCENTE};
  public heads: Array<HeadTableComponent>;
  private countClickSort:number = 1;
  public itemSelected: any;
  public listSelected: any;
  private idActionDelete = "idActionDeleteTable";
  private idActionEdit = "idActionEditTable";
  private idActionPesq = "idActionPesqTable";


  public addHeads(head: HeadTableComponent){
    this.heads.push(head);
  }
  
  ngOnInit() {
  }

  setLimit(select){
    let novoLimit = select.value;
    if(this.pagination.limit != novoLimit)
    this.pagination.limit = novoLimit;
    this.emitirPagination();
  }
  
  pageNext(){
    this.app.cleanActions();
    this.pagination.page = this.data.page;
    this.pagination.page = ++this.pagination.page;
    this.emitirPagination();
  }
  
  pageBack(){
    this.pagination.page = this.data.page;
    this.pagination.page = --this.pagination.page;
    this.emitirPagination();
  }
  
  setSort(head){
    if(head.sort){
      this.countClickSort++;
      if(head.field != this.pagination.fieldSort){
        this.pagination.fieldSort = head.field;
        this.countClickSort = 1;
        this.pagination.sort = this.CRESCENTE;
      }
      if(this.countClickSort == 3){
        this.pagination.fieldSort = this.FIELD_SORT_DEFAULT;
        this.countClickSort = 0;
      }
      this.emitirPagination();
      this.pagination.sort = (this.pagination.sort == this.CRESCENTE) ? this.DECRESCENTE : this.CRESCENTE;
    }
  }
  
  clickRow(item:any){
    if(!this.isMult){
      if(this.itemSelected && this.itemSelected["selected"]){
        if(this.itemSelected == item){
          item.selected = false;
          this.itemSelected = null;
        }else{
          this.itemSelected["selected"] = false;
          item.selected = true;
          this.itemSelected = item;
        }
      }else{
        item.selected = true;
        this.itemSelected = item;
      }
      this.onClick.emit(this.itemSelected);
    }else{
      if(item["selected"]){
        item["selected"] = false;
      }else{
        item["selected"] = true;
      }
      this.listSelected = this.list || this.data[oMsg.LIST];
      this.listSelected = this.listSelected.filter( (item) => {return item["selected"]});
      this.onClick.emit(this.listSelected);
    }
  }

  reload(){
    this.emitirPagination()
  }
  
  emitirPagination(){
    this.loadTable.emit(this.pagination);
  }
}
