import {
  Component,
  OnInit,
  Input,
  Optional,
  Host,
  ElementRef,
  Renderer2
} from '@angular/core';
import { TableComponent } from './../table.component';

@Component({
  selector: 'm-head-table',
  templateUrl: './head-table.component.html',
  styleUrls: ['./head-table.component.scss']
})
export class HeadTableComponent implements OnInit {

  constructor(
    @Optional() @Host() private table: TableComponent,
    private elementRef: ElementRef,
    private renderer2: Renderer2
  ) { }

  @Input() label: string;
  @Input() field: string;
  @Input() sort: boolean = false;
  @Input() colDelete: boolean = false;
  @Input() colEdit: boolean = false;
  @Input() addClass: string;
  @Input() width: string;

  ngOnInit() {
    this.table.addHeads(this);
  }

}
