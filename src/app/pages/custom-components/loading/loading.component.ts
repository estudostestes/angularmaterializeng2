import { ngIfFade } from './../../../util/animates.custons';
import { MzBaseModal } from 'ng2-materialize';
import { Component, OnInit, ElementRef, Renderer2} from '@angular/core';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss'],
  animations: [ngIfFade]
  
})
export class LoadingComponent extends MzBaseModal implements OnInit {
  constructor(
    private element: ElementRef,
    private renderer: Renderer2
  ){
    super();
  }
  modalOptions: Materialize.ModalOptions = {dismissible: false, inDuration: 50};
  static showMessage = true;
  static message: string;

  ngOnInit() {
  }

  public close(){
    this.modalComponent.close();
  }

  get staticMessage(){
    return LoadingComponent.message;
  }

  get showMessage(){
    return LoadingComponent.showMessage;
  }

  public static setMessage(message: string){
    LoadingComponent.message = message;
  }
  
}
