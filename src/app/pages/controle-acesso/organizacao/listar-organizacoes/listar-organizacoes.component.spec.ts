import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarOrganizacoesComponent } from './listar-organizacoes.component';

describe('ListarOrganizacoesComponent', () => {
  let component: ListarOrganizacoesComponent;
  let fixture: ComponentFixture<ListarOrganizacoesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListarOrganizacoesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarOrganizacoesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
