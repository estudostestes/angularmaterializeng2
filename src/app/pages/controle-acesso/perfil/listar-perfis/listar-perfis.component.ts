import { ngIfFade } from './../../../../util/animates.custons';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { URLs, oMsg } from './../../../../util/values';
import { AppComponent } from './../../../../app.component';
import { DialogService } from './../../../../services/dialog.service';
import { TranslateService } from './../../../../util/translate/translate.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-listar-perfis',
  templateUrl: './listar-perfis.component.html',
  styleUrls: ['./listar-perfis.component.scss'],
  animations: [ngIfFade]
})
export class ListarPerfisComponent implements OnInit {

  constructor(
    private translateService: TranslateService,
    private dialogService: DialogService,
    private http: HttpClient,
    private router: Router,
    private app: AppComponent
  ) { }

  ngOnInit() {
    let titulo = this.translateService.getValue("TITLE_PERFIL");
    this.textBtnPesq = this.translateService.getValue("PESQUISAR");
    this.app.setTitle(titulo);
    this.app.addActionFixed("idSearchPerfil", "search", this.textBtnPesq, () => {
      this.showFiltro = !this.showFiltro;
    });
    this.listarFiltros();
  }
  public dataTable:any;
  public fieldsSearch = {nome: "", descricao: "", organizacao: ""};
  public search = {};
  public idBtnRemover= "idBtnRemoverPerfil";
  public idBtnEdit= "idBtnEditPerfil";
  public itemSelected: any;
  private textRemover = this.translateService.getValue("REMOVER");
  private textEditar = this.translateService.getValue("EDITAR");
  public textBtnPesq;
  public showFiltro: boolean = false;

  selected(item:any){
    if(item){
      console.log(item);
      this.app.addAction(this.idBtnRemover, "delete", this.textRemover,() => {
        this.remove();
      });
      
      this.app.addAction(this.idBtnEdit, "edit", this.textEditar,() => {
        this.edit();
      });
    }else{
      this.removeBtnItens();
    }
    this.itemSelected = item;
  }
  
  remove(){
    let nomeFantaisa = this.itemSelected["nome"];
    let mensagem = this.translateService.getValue("DESEJA_EXCLUIR", nomeFantaisa);
    this.dialogService.confirm(null, mensagem, () => {
      this.removeItem(this.itemSelected["_id"]);
    }, null);
  }

  removeItem(id: any){
    this.dialogService.openLoading(null, 0);
    this.http.delete(URLs.localhost + URLs.PERFIL + "/" + id).subscribe(data => {
      this.dialogService.closeLoading();
      this.dialogService.showMsgData(data);
      this.listarFiltros();
      }, error => {
        this.dialogService.showErrorModal(error);
    });
  }


  edit(){
    this.router.navigate(['/cad-organizacao', this.itemSelected["_id"]]);
  }

  setPagination(search: any){
    this.search = search;
    this.listarFiltros();
  }
  
  limparFiltro(){
    this.fieldsSearch = {nome: "", descricao: "", organizacao: ""};
  }

  listarFiltros(){
    this.dialogService.openLoading(null, 0);
    this.search[oMsg.SEARCH] = this.fieldsSearch;
    this.http.post(URLs.localhost + URLs.PERFIL_PAGINATION, this.search).subscribe(data =>{
      this.dataTable = data;
      this.dialogService.closeLoading();
      console.log(data);
    },error => {
      this.dialogService.showErrorModal(error);
    });
    this.removeBtnItens();
  }
 
  removeBtnItens(){
    this.app.removeAction(this.idBtnEdit);
    this.app.removeAction(this.idBtnRemover);
  }

  close(isShow){
    this.showFiltro = isShow;
  }
  

}
