import { DialogService } from './../../../services/dialog.service';
import { ngIfFade } from './../../../util/animates.custons';
import { AppComponent } from './../../../app.component';
import { URLs, tMsg, oMsg } from './../../../util/values';
import {
   Component,
    OnInit,
    AfterViewChecked
} from '@angular/core';
import { HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-listar-usuarios',
  templateUrl: './listar-usuarios.component.html',
  styleUrls: ['./listar-usuarios.component.scss'],
  animations: [ngIfFade]
})
export class ListarUsuariosComponent implements OnInit, AfterViewChecked {

  constructor(
    private app: AppComponent,
    private http: HttpClient,
    private dialogService: DialogService
  ) { }
  
  usuarios = [];

  ngOnInit() {
    this.listarUsuarios();
  }
  
  ngAfterViewChecked(){
  }

  async listarUsuarios(){
    this.dialogService.openLoading(null, 0);
    this.app.setTitle("Usuários");
    this.http.get(URLs.localhost + URLs.USUARIO).subscribe(data => {
      let usuarios = data[oMsg.LIST];
      this.usuarios = usuarios;
      this.dialogService.closeLoading();
    });
  }

}
