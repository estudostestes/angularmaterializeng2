import { DialogService } from './../../services/dialog.service';
import { TranslateService } from './../../util/translate/translate.service';
import { AppComponent } from './../../app.component';
import {
   Component,
   OnInit,
   AfterViewChecked,
} from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, AfterViewChecked {

  constructor(
    private app: AppComponent,
    private translateService: TranslateService,
    private dialogService: DialogService
  ) { }
  
  ngOnInit() {
    let home = this.translateService.getValue("HOME");
    this.app.setTitle(home);
  }
  
  ngAfterViewChecked(){
    
  }

  public showAlert(){
    this.dialogService.openLoading("Teste de loading", 0);
    setTimeout(() => {
      this.dialogService.setMessageLoading("Continuando o texto!");
    }, 3000);
  }
}
