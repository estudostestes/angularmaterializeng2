export const URLs = {//Urls rest.
    "localhost": "http://localhost:3000",
    //Auth
    "AUTH_LOGIN": "/rest/getLoginAuth",
    "AUTH_AUTHENTICATE": "/rest/authenticate",
    "AUTH_LOGGED": "/rest/authLogged",
    "AUTH_ALL": "/*",
    //Usuário
    "USUARIO": "/rest/usuario",
    "USUARIO_ PAGINATION": "/rest/usuario/pagination",
    "USUARIO_ FILTER": "/rest/usuario/filter",
    //Organizacao
    "ORGANIZACAO": "/rest/organizacao",
    "ORGANIZACAO_PAGINATION": "/rest/organizacao/pagination",
    "ORGANIZACAO_FILTER": "/rest/organizacao/filter",
    //Filial
    "FILIAL": "/rest/filial",
    "FILIAL_PAGINATION": "/rest/filial/pagination",
    "FILIAL_FILTER": "/rest/filial/filter",
    //Perfil
    "PERFIL": "/rest/perfil",
    "PERFIL_PAGINATION": "/rest/perfil/pagination",
    "PERFIL_FILTER ": "/rest/perfil/filter",
    //Acao
    "ACAO": "/rest/acao",
    "ACAO_PAGINATION": "/rest/acao/pagination",
    "ACAO_FILTER":  "/rest/acao/filter",
    //Acesso
    "ACESSO": "/rest/acesso",
    "ACESSO_PAGINATION": "/rest/ace,sso/pagination",
    "ACESSO_FILTER": "/rest/filter"
}

export const tMsg = {//Tipos de mensagens
    "DANGER": "msgErro",
    "SUCCESS": "msgSuccesso",
    "INFO": "msgInfo",
    "ALERT": "msgAlert"
}

export const oMsg = {//Objetos de retorno
    "OBJ": "obj",
    "LIST": "list",
    "SEARCH": "search",
    "LIST_MSG": "listMsg",
    "PAGE": "page",
    "TOTAL_PAGES": "totalPages",
    "FIELD_SORT": "fieldSort",
    "SORT": "sort",
    "LIMIT": "limit",
    "RANGE_START": "rangeStart",
    "RANGE_END": "rangeEnd",
    "TOTAL_ROWS": "totalRows",
}

export const secretToken ={
    "TOKEN": "x-access-token",
    "SECRET_PUBLIC" : "projeto-publico"
}