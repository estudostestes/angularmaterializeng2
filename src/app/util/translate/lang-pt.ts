export const LANG_PT_NAME = 'pt';

export const LANG_PT_TRANS = {
    //-------------------------------------------------------- Telas
    // --------------------Botoes
    "LIMPAR": "Limpar",
    "REMOVER": "Remover",
    "EDITAR": "Editar",
    "CANCELAR": "Cancelar",
    "PESQUISAR": "Pesquisar",
    "PROXIMO": "Próximo",
    "SALVAR": "Salvar",
    "VOLTAR": "Voltar",
    "SAIR": "Sair",
    //---------------------labels
    //------Login
    "ORGANIZACAO": "Organização",
    "ENTRAR": "Entrar",
    "ERRO": "Erro",
    "SEM_REGISTROS": "Nenhum registro encontrado.",
    "MENSAGEM": "Mensagem",
    "ATENCAO": "Atenção",
    "BEM_VINDO": "Bem vindo(a)",
    "DIGITE_LOGIN": "Digite seu login",
    "DIGITE_SENHA": "Digite sua Senha",
    "ESQUECEU_LOGIN": "Esqueceu seu login?",
    "ESQUECEU_SENHA": "Esqueceu sua senha?",
    "AGUARDE_LOGIN": "Aguarde.. validando login",
    "NOME_FANTASIA": "Nome Fantasia",
    "CNPJ": "CNPJ",
    "AGUARDE": "Aguarde",
    "NOME": "Nome",
    "DESCRICAO": "Descricão",
    

    // ---------------- Titulos
    "LOGIN": "Login",
    "TITLE_ORGANIZACOES": "Organizações",
    "TITLE_CAD_ORGANIZACAO": "Cadastrar Organização",
    "TITLE_EDIT_ORGANIZACAO": "Editar Organização",
    "TITLE_PERFIL": "Perfis",
    "TITLE_CAD_PERFIL": "Cadastrar Perfil",
    "TITLE_EDIT_PERFIL": "Editar Perfil",
    

//------------------------------------------------------------ Mensagens Servidor
// ------------------- Login
"LOGIN_INVALIDO" : "Login não cadastradao!",
"SENHA_INVALIDA": "A senha informada é inválida!",
// ------------------- Organização
"ORGANIZACAO_INSERIDA_COM_SUCESSO": "Organização cadastrada com sucesso!",
"ORGANIZACAO_ATUALIZADA_COM_SUCESSO": "Organização atualizado com sucesso!",
"DESEJA_EXCLUIR": "Deseja excluir %0",
//------------------------------------------------------------ Erros de stastus do servidor
    "statusDefault": "Erro ao tentar conectar com servidor.",
    "status0": "Você parece está offline, verique sua internet.",
    "status302": "O recurso foi movido temporariamente para outra URI.",
    "status304": "O recurso não foi alterado.",
    "status400": "Os dados da requisição não são válidos.",
    "status401": "A URI especificada exige autenticação do cliente.",
    "status403": "O servidor entende a requisição, mas se recusa em atendê-la. O cliente não deve tentar fazer uma nova requisição.",
    "status404": "O servidor não encontrou nenhuma URI correspondente.",
    "status405": "O método especificado na requisição não é válido na URI. A resposta deve incluir um cabeçalho Allow com uma lista dos métodos aceitos.",
    "status410": "O recurso solicitado está indisponível mas seu endereço atual não é conhecido.",
    "status500": "O servidor não foi capaz de concluir a requisição devido a um erro inesperado.",
    "status502": "O servidor, enquanto agindo como proxy ou gateway, recebeu uma resposta inválida do servidor upstream a que fez uma requisição.",
    "status503": "O servidor, enquanto agindo como proxy ou gateway, recebeu uma resposta inválida do servidor upstream a que fez uma requisição.",
};