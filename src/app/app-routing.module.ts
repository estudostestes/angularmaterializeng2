import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CanActivate } from '@angular/router';
import { AuthGuard } from './services/auth.guard';
//Páginas
import { LoginComponent } from './pages/login/login.component';
import { HomeComponent } from './pages/home/home.component';
import { ListarUsuariosComponent } from './pages/usuario/listar-usuario/listar-usuarios.component';
import { CadastroOrganizacaoComponent } from './pages/controle-acesso/organizacao/cadastro-organizacao/cadastro-organizacao.component';
import { ListarOrganizacoesComponent } from './pages/controle-acesso/organizacao/listar-organizacoes/listar-organizacoes.component';
import { CadastroPerfilComponent } from './pages/controle-acesso/perfil/cadastro-perfil/cadastro-perfil.component';
import { ListarPerfisComponent } from './pages/controle-acesso/perfil/listar-perfis/listar-perfis.component';

const routes: Routes = [
  //Rotas principais
  { path: 'login', component: LoginComponent},
  { path: '', component: HomeComponent, canActivate: [AuthGuard]},
  { path: 'home', component: HomeComponent, canActivate: [AuthGuard]},
  { path: 'usuario', component: ListarUsuariosComponent, canActivate: [AuthGuard]},
  //,{ path: '**', component: LoginComponent}
  // Rotas controle de acesso
  {path: 'organizacao', component: ListarOrganizacoesComponent},
  { path: 'cad-organizacao/:id', component: CadastroOrganizacaoComponent, canActivate: [AuthGuard]},
  {path: 'perfil', component: ListarPerfisComponent},
  { path: 'cad-perfil', component: CadastroPerfilComponent, canActivate: [AuthGuard]},
  { path: 'cad-perfil/:id', component: CadastroPerfilComponent, canActivate: [AuthGuard]},
  
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
  ],
  exports: [
    RouterModule
  ],
  providers:[]
})
export class AppRoutingModule { }
